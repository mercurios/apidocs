# API docs

This repository contains the detailed documentation on how to use the API provided by [Mainframe](https://gitlab.com/mercurios/mainframe), and the [docusaurus](https://docusaurus.io) web application that serves it.

[Mercurio](https://gitlab.com/mercurios) // [Mainframe](https://gitlab.com/mercurios/mainframe)

You can read the documentation directly from the `docs` folder, or read it online at [mercurios.gitlab.io/apidocs](https://mercurios.gitlab.io/apidocs).
