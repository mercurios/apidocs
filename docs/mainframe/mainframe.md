---
id: mainframe
title: Meet Mainframe
sidebar_label: API Specification
---

Mercurio is built following an architecture inspired by [mainframe computers](https://en.wikipedia.org/wiki/Mainframe_computer) and has 3 different applications that can compare to a mainframe system; *Mainframe*: the app that provides the public API to interact with the database; *Terminal*: a library to develop applications that connect with the API and *Monitor*: the main interface app for the whole Mercurio app.

[Mainframe](https://gitlab.com/mercurios/mainframe), being it's role that of the API provider, is at the very center of the Mercurio project and most of the environment spins around it.

This API is a [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)ful, [CRUD](https://en.wikipedia.org/wiki/Representational_state_transfer) system that makes it available and easy to interact with the Mercurio data. While Mainframe itself is also a [Symfony](http://symfony.com) app, this documentation won't enter into details about Mainframe and the implementation of the API here described, **this documentation is about the API**, not the app.

Mainframe uses HTTP to listen for requests and sends back status codes, data is sent encoded as JSON.

## Requests
Requests are made of work data, or **payload**, which is the data you want to insert or modify into the database, and control data, or **credentials**, which is the data you use to validate your requests against the system.

>A single invalid or malformed payload or credential will invalidate the entire request.

Both parts of the request should be sent in the body of the request, and depending on the type of request, they should be sent as:

| METHOD | ACTION | TAKES PARAMETERS FROM                       |
|--------|--------|---------------------------------------------|
| GET    | Read   | Query parameters.                           |
| POST   | Create | x-www-form-urlencoded / multipart/form-data |
| PUT    | Update | x-www-form-urlencoded / multipart/form-data |
| DELETE | Remove | x-www-form-urlencoded / multipart/form-data |

### Payload
The payload parameters will always match the names of the entity data they represent and have strict constraints their data needs to meet in order for the request to be validated and perform the desired action.

Example minimum payload to create a client:
```JSON
{
    "name": "Example client app",
    "url": "http://clientapp.com",
    "devMail": "mail@clientapp.com"
}
```

### Credentials
Credentials are control values used by Mainframe to validate requests and keep track of actions. These values are actual entity data in the database, but when used as credentials their name don't match their actual entity property name.

There are two pairs of credentials:

1. **Client credentials**: a client `clientId` and `clientSecret` used to identify client apps doing requests and to allow the client owners to modify their client.
Ids are used to publicly identify entities, however the secrets are for the owners of each client only, i.e: **keep your clients secrets a secret**. Think of them as passwords, as they pretty much are.

2. **User credentials**: a Google `googleClientId` and `googleIdToken` used to authenticate users who signed in using their google accounts via a client app of both Mainframe (identified by the client credentials) and Google APIs (googleClientId).

## Responses
Mainframe returns standarised JSON responses along with a HTTP status code.

### Success
Requests that are successful will return a `200` (success) or `201` (created) status code. The JSON content is wrapped inside a `data` key.

Example succesful response for POST /client:
```JSON
{
    "data": {
        "client": {
            "_secret": "bf1ad41009617808e2dcb5e037385ded3ae7686f6300a66e7714677d4f640b70",
            "name": "Example client app",
            "url": "http://clientapp.com",
            "icon": "",
            "devMail": "mail@clientapp.com",
            "_id": "2648635454491983872",
            "_timestamp": 1578165101,
            "_updated": 1578165101
        }
    }
}
```

>Properties that start with an underscore, for instance: _id, _timestamp, etc., are *system set properties* that can't be modified. Including these in your payload will not invalidate the request, but they will be ignored.

### Error
Requests that are invalid or that can't return results, will result a `400` or `404` HTTP status code. The JSON content is wrapped inside an `error` key and will return an error detail for each failure.

Example error response for POST /client:
```JSON
{
    "error": {
        "url": "This value is too short. It should have 3 characters or more.",
        "devMail": "This value is not a valid email address."
    }
}
```

### Failure
On the hopefully rare case of API failures and/or undocumented behaviour, you will receive a `500` status code, or other similar status, sent by the server itself rather than Mainframe. No JSON response will be returned.
